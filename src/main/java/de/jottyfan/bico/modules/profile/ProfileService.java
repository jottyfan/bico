package de.jottyfan.bico.modules.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.bico.modules.profile.model.ProfileBean;

/**
 *
 * @author jotty
 *
 */
@Service
public class ProfileService {

	@Autowired
	private ProfileRepository repository;

	/**
	 * get the theme of the user
	 *
	 * @param username the name of the user
	 * @return the theme of the user
	 */
	public String getTheme(String username) {
		ProfileBean bean = repository.getProfile(username);
		return bean == null ? "light" : bean.getTheme();
	}

	/**
	 * update the theme of the user
	 *
	 * @param username the name of the user
	 * @param theme the theme; may be light or dark
	 */
	public void updateTheme(String username, String theme) {
		repository.upsertTheme(username, theme);
	}

}
