package de.jottyfan.bico.modules.profile.model;

import java.io.Serializable;

/**
 *
 * @author jotty
 *
 */
public class ProfileBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String username;
	private String theme;

	public static final ProfileBean of(String username) {
		ProfileBean bean = new ProfileBean();
		bean.setUsername(username);
		return bean;
	}

	public static final ProfileBean of(String username, String theme) {
		return ProfileBean.of(username).withTheme(theme);
	}

	public ProfileBean withTheme(String theme) {
		this.theme = theme;
		return this;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the theme
	 */
	public String getTheme() {
		return theme;
	}

	/**
	 * @param theme the theme to set
	 */
	public void setTheme(String theme) {
		this.theme = theme;
	}
}
