package de.jottyfan.bico.modules.profile;

import static de.jottyfan.bico.db.Tables.T_PROFILE;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.SelectConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.bico.db.tables.records.TProfileRecord;
import de.jottyfan.bico.modules.profile.model.ProfileBean;

/**
 *
 * @author jotty
 *
 */
@Repository
public class ProfileRepository {
	private static final Logger LOGGER = LogManager.getLogger(ProfileRepository.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * get the profile of the user
	 *
	 * @param username the name of the user
	 * @return the profile or null if not found
	 */
	public ProfileBean getProfile(String username) {
		SelectConditionStep<TProfileRecord> sql = jooq
		// @formatter:off
			.selectFrom(T_PROFILE)
			.where(T_PROFILE.USERNAME.eq(username));
		// @formatter:on
		LOGGER.trace(sql.toString());
		TProfileRecord r = sql.fetchOne();
		if (r != null) {
			return ProfileBean.of(username).withTheme(r.getTheme());
		} else {
			return null;
		}
	}

	/**
	 * upsert theme for username
	 *
	 * @param username the name of the user
	 * @param theme    the selected theme; may be light or dark
	 * @return number of affected database rows; should be 1
	 */
	public Integer upsertTheme(String username, String theme) {
		InsertOnDuplicateSetMoreStep<TProfileRecord> sql = jooq
		// @formatter:off
			.insertInto(T_PROFILE,
					        T_PROFILE.USERNAME,
					        T_PROFILE.THEME)
			.values(username, theme)
			.onConflict(T_PROFILE.USERNAME)
			.doUpdate()
			.setAllToExcluded();
		// @formatter:on
		LOGGER.trace(sql.toString());
		return sql.execute();
	}
}
