package de.jottyfan.bico.modules.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import de.jottyfan.bico.modules.CommonController;

/**
 *
 * @author jotty
 *
 */
@RestController
public class ProfileController extends CommonController {

	@Autowired
	private ProfileService service;

	/**
	 * update the theme of the current user
	 *
	 * @param theme the theme
	 */
	@PostMapping("/updateTheme/{theme}")
	public ResponseEntity<?> updateTheme(@PathVariable String theme) {
		// TODO: add profile's user name
		String username = "jotty";
		service.updateTheme(username, theme);
		return ResponseEntity.ok(null);
	}

}
