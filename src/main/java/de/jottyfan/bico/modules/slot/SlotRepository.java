package de.jottyfan.bico.modules.slot;

import static de.jottyfan.bico.db.Tables.T_LESSON;
import static de.jottyfan.bico.db.Tables.T_SLOT;

import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertResultStep;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.bico.db.tables.records.TLessonRecord;
import de.jottyfan.bico.db.tables.records.TSlotRecord;
import de.jottyfan.bico.modules.slot.model.SlotBean;

/**
 *
 * @author jotty
 *
 */
@Repository
public class SlotRepository {
	private static final Logger LOGGER = LogManager.getLogger(SlotRepository.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * get the slot referenced by id
	 *
	 * @param id the pkSlot value
	 * @return the slot or null
	 */
	public SlotBean getSlot(Integer id) {
		SelectConditionStep<TSlotRecord> sql = jooq
		// @formatter:off
			.selectFrom(T_SLOT)
			.where(T_SLOT.PK_SLOT.eq(id));
		// @formatter:on
		LOGGER.trace(sql);
		Iterator<TSlotRecord> i = sql.fetch().iterator();
		SlotBean bean = null;
		while (i.hasNext()) {
			TSlotRecord r = i.next();
			bean = new SlotBean();
			bean.setPkSlot(r.getPkSlot());
			bean.setSlotDay(r.getSlotDay());
			bean.setNote(r.getNote());
		}
		return bean;
	}

	/**
	 * add a slot and return is new ID
	 *
	 * @param slot the slot
	 * @return the ID of the slot
	 */
	public Integer addSlot(SlotBean slot) {
		InsertResultStep<TSlotRecord> sql = jooq
		// @formatter:off
			.insertInto(T_SLOT,
					        T_SLOT.SLOT_DAY,
					        T_SLOT.NOTE)
			.values(slot.getSlotDay(), slot.getNote())
			.returning(T_SLOT.PK_SLOT);
		// @formatter:on
		LOGGER.trace(sql);
		return sql.fetchOne().getPkSlot();
	}

	/**
	 * update the slot
	 *
	 * @param slot the slot
	 */
	public void changeSlot(SlotBean slot) {
		UpdateConditionStep<TSlotRecord> sql = jooq
		// @formatter:off
			.update(T_SLOT)
			.set(T_SLOT.SLOT_DAY, slot.getSlotDay())
			.set(T_SLOT.NOTE, slot.getNote())
			.where(T_SLOT.PK_SLOT.eq(slot.getPkSlot()));
		// @formatter:on
		LOGGER.trace(sql);
		sql.execute();
	}

	/**
	 * remove slot
	 *
	 * @param id the ID of the slot
	 */
	public void deleteSlot(Integer id) {
		DeleteConditionStep<TSlotRecord> sql = jooq
		// @formatter:off
			.deleteFrom(T_SLOT)
			.where(T_SLOT.PK_SLOT.eq(id));
		// @formatter:on
		LOGGER.trace(sql);
		sql.execute();
	}

	/**
	 * get the slot only if it is not yet used by another table
	 *
	 * @param id the ID of the slot
	 * @return the slot or null
	 */
	public SlotBean getSlotIfDeletable(Integer id) {
		SelectConditionStep<TLessonRecord> sql = jooq
		// @formatter:off
			.selectFrom(T_LESSON)
			.where(T_LESSON.FK_SLOT.eq(id));
		// @formatter:on
		LOGGER.trace(sql);
		if (sql.fetchOne() == null) {
			return getSlot(id);
		} else {
			return null;
		}
	}
}
