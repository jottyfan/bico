package de.jottyfan.bico.modules.slot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.bico.modules.slot.model.SlotBean;

/**
 *
 * @author jotty
 *
 */
@Service
public class SlotService {

	@Autowired
	private SlotRepository repository;

	public SlotBean loadSlot(Integer id) {
		return repository.getSlot(id);
	}

	public Integer saveSlot(SlotBean slot) {
		if (slot.getPkSlot() == null) {
			return repository.addSlot(slot);
		} else {
			repository.changeSlot(slot);
			return slot.getPkSlot();
		}
	}

	public void removeSlot(Integer id) {
		repository.deleteSlot(id);
	}

	public SlotBean loadDeletableSlot(Integer id) {
		return repository.getSlotIfDeletable(id);
	}
}
