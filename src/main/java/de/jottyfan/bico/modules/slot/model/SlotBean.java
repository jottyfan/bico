package de.jottyfan.bico.modules.slot.model;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotNull;

/**
 *
 * @author jotty
 *
 */
// TODO: not exists validator that checks for another pkSlot not to have that slotDay
public class SlotBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer pkSlot;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull(message = "Das Datum muss ausgefüllt werden.")
	private LocalDate slotDay;

	private String note;

	public SlotBean withNote(String note) {
		this.note = note;
		return this;
	}

	/**
	 * @return the pkSlot
	 */
	public Integer getPkSlot() {
		return pkSlot;
	}

	/**
	 * @param pkSlot the pkSlot to set
	 */
	public void setPkSlot(Integer pkSlot) {
		this.pkSlot = pkSlot;
	}

	/**
	 * @return the slotDay
	 */
	public LocalDate getSlotDay() {
		return slotDay;
	}

	/**
	 * @param slotDay the slotDay to set
	 */
	public void setSlotDay(LocalDate slotDay) {
		this.slotDay = slotDay;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}
}
