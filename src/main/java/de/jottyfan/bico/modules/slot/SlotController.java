package de.jottyfan.bico.modules.slot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import de.jottyfan.bico.modules.CommonController;
import de.jottyfan.bico.modules.slot.model.SlotBean;
import jakarta.validation.Valid;

/**
 *
 * @author jotty
 *
 */
@Controller
public class SlotController extends CommonController {

	@Autowired
	private SlotService service;

	@GetMapping("/slot")
	public String generate(Model model) {
		return load(null, model);
	}

	@GetMapping("/slot/{id}")
	public String load(@PathVariable Integer id, Model model) {
		model.addAttribute("bean", id == null ? new SlotBean() : service.loadSlot(id));
		return "/slot/item";
	}

	@GetMapping("/slot/{id}/delete")
	public String loadEnsurance(@PathVariable Integer id, Model model) {
		model.addAttribute("bean", service.loadDeletableSlot(id));
		return "/slot/delete";
	}

	@GetMapping("/slot/{id}/destroy")
	public String destroy(@PathVariable Integer id, Model model) {
		service.removeSlot(id);
		return "redirect:/";
	}

	@PostMapping("/slot/save")
	public String save(@Valid @ModelAttribute("bean") SlotBean bean, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "/slot/item";
		}
		service.saveSlot(bean);
		return "redirect:/sheet";
	}
}
