package de.jottyfan.bico.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import de.jottyfan.bico.modules.profile.ProfileService;

/**
 *
 * @author jotty
 *
 */
public abstract class CommonController {

	@Autowired
	private ProfileService profileService;

	/**
	 * get the theme for the current session
	 *
	 * @return the theme; light or dark at the moment
	 */
	public Model useThemedModel(Model model) {
		// TODO: add profile's user name
		String username = "jotty";
		model.addAttribute("theme", profileService.getTheme(username));
		return model;
	}
}
