package de.jottyfan.bico.modules.sheet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.bico.db.tables.records.VCalendarRecord;

/**
 *
 * @author jotty
 *
 */
@Service
public class SheetService {

	@Autowired
	private SheetRepository repository;

	public List<VCalendarRecord> getList() {
		return repository.getList();
	}

}
