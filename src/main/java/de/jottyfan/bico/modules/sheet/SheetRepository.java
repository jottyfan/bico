package de.jottyfan.bico.modules.sheet;

import static de.jottyfan.bico.db.Tables.V_CALENDAR;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.SelectWhereStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.bico.db.tables.records.VCalendarRecord;

/**
 *
 * @author jotty
 *
 */
@Repository
public class SheetRepository {

	private static final Logger LOGGER = LogManager.getLogger(SheetRepository.class);

	@Autowired
	private DSLContext jooq;

	public List<VCalendarRecord> getList() {
		 SelectWhereStep<VCalendarRecord> sql = jooq.selectFrom(V_CALENDAR);
		LOGGER.trace(sql);
		return sql.fetch().stream().toList();
	}
}
