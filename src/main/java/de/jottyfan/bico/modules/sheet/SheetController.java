package de.jottyfan.bico.modules.sheet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import de.jottyfan.bico.modules.CommonController;

/**
 *
 * @author jotty
 *
 */
@Controller
public class SheetController extends CommonController {

	@Autowired
	private SheetService service;

	@GetMapping("/sheet")
	public String getSheet(Model model) {
		useThemedModel(model).addAttribute("list", service.getList());
		return "/sheet";
	}
}
