package de.jottyfan.bico.modules.index;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import de.jottyfan.bico.modules.CommonController;

/**
 *
 * @author jotty
 *
 */
@Controller
public class IndexController extends CommonController {
	@GetMapping("/")
	public String getIndex(Model model) {
		useThemedModel(model);
		return "redirect:/sheet";
	}
}
