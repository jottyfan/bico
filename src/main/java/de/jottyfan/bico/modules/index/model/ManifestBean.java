package de.jottyfan.bico.modules.index.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author jotty
 *
 */
@Component
public class ManifestBean {

	@Autowired(required = false)
	private BuildProperties buildProperties;

	/**
	 * @return the version of this project
	 */
	public String getVersion() {
		return buildProperties != null ? buildProperties.getVersion()
				: this.getClass().getPackage().getImplementationVersion();
	}
}
