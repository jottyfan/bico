toggleDarkMode = function() {
	var oldValue = $("html").attr("data-bs-theme");
	var newValue = oldValue == "dark" ? "light" : "dark";
	var updateUrl = /*[[@{/updateTheme}]]*/ 'updateTheme';
	updateUrl = updateUrl + "/" + newValue;
	$("html").attr("data-bs-theme", newValue);
	$.ajax({
		url: updateUrl,
		type: "POST",
		contentType: "application/json",
		dataType: 'json'
	});
}

/**
 * only because th:data-bs-theme="${theme}" does not work
 */
$(document).ready(function(){
	var theme = /*[[${theme}]]*/ 'light';
	$("html").attr("data-bs-theme", theme);
});
